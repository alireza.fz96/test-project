import { useEffect, useState } from 'react';
import getPosts from '../api';
import { Post } from '../models/Post';

export default function usePosts(page: number, searchText: string) {
	const [posts, setPosts] = useState<Post[]>([]);
	const [loading, setLoading] = useState<boolean>(true);
	const [error, setError] = useState<string>('');

	useEffect(() => {
		setLoading(true);
		setError('');
		getPosts(page, searchText)
			.then(data => setPosts(data))
			.catch(error => {
				setError(error);
			})
			.finally(() => {
				setLoading(false);
			});
	}, [page, searchText]);

	return { posts, loading, error };
}
