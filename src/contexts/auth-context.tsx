import { createContext } from 'react';

const defaultValue = {
	isAuthenticated: false,
	login: (email: string, password: string) => {},
	logout: () => {}
};

const AuthContext = createContext(defaultValue);

export default AuthContext;
