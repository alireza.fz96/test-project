import { Box, Container, Grid } from '@material-ui/core';
import { useCallback, useState } from 'react';
import PostCard from '../../components/post-card/post-card';
import usePosts from '../../hooks/usePosts';
import Pagination from '@material-ui/lab/Pagination';
import Skeleton from '@material-ui/lab/Skeleton';
import SearchBar from '../../components/search-bar/search-bar';

export default function Posts() {
	const [page, setPage] = useState<number>(1);
	const [searchText, setSearchText] = useState<string>('');

	const { posts, loading } = usePosts(page, searchText);

	const handleChangePage = (event: any, value: number) => {
		setPage(value);
	};

	const handleChangeSearchText = useCallback((event: any) => {
		const { value } = event.target;
		setSearchText(value);
		if (!value) setPage(1);
	}, []);

	return (
		<Box my={5}>
			<Container>
				<Box mb={5}>
					<SearchBar onChange={handleChangeSearchText} />
				</Box>
				<Grid container spacing={2}>
					{loading ? (
						Array.from(new Array(10)).map((_, index) => (
							<Grid item xs={6} sm={4} md={3} key={index}>
								<Skeleton variant="text" />
								<Skeleton variant="circle" width={40} height={40} />
								<Skeleton variant="rect" width="100%" height={118} />
							</Grid>
						))
					) : posts.length > 0 ? (
						posts.map(post => (
							<Grid item xs={6} sm={4} md={3} key={post.id}>
								<PostCard post={post} />
							</Grid>
						))
					) : (
						<Grid item xs={12}>
							<Box textAlign="center">No Post Founded !</Box>
						</Grid>
					)}
				</Grid>
				{posts.length > 0 && (
					<Box mt={4} pb={2} display="flex" justifyContent="center">
						<Pagination
							count={10}
							color="primary"
							page={page}
							onChange={handleChangePage}
						/>
					</Box>
				)}
			</Container>
		</Box>
	);
}
