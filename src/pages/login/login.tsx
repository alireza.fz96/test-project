import classes from './login.module.scss';
import { Box, Button, TextField } from '@material-ui/core';
import { useFormik } from 'formik';
import * as yup from 'yup';
import { useContext } from 'react';
import AuthContext from '../../contexts/auth-context';
import { useHistory } from 'react-router-dom';

const validationSchema = yup.object({
	email: yup.string().email('Enter a valid email').required('Enter your email'),
	password: yup
		.string()
		.required('Enter your Password')
		.min(8, 'Password Length must be more than 8 characters')
});

interface Values {
	email: string;
	password: string;
}

export default function Login() {
	const auth = useContext(AuthContext);
	const history = useHistory();

	const formik = useFormik({
		initialValues: { email: '', password: '' },
		validationSchema,
		onSubmit: (values: Values) => {
			const { email, password } = values;
			auth.login(email, password);
			history.replace('/products');
		}
	});

	return (
		<div className={classes['form-container']}>
			<form onSubmit={formik.handleSubmit} className={classes.form}>
				<Box mb={2}>
					<TextField
						value={formik.values.email}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						name="email"
						id="email"
						type="email"
						error={formik.touched.email && !!formik.errors.email}
						helperText={formik.touched.email && formik.errors.email}
						color="primary"
						variant="outlined"
						label="Email"
						placeholder="email@email.com"
						fullWidth
					/>
				</Box>
				<Box mb={5}>
					<TextField
						value={formik.values.password}
						onChange={formik.handleChange}
						onBlur={formik.handleBlur}
						name="password"
						id="password"
						type="password"
						error={formik.touched.password && !!formik.errors.password}
						helperText={formik.touched.password && formik.errors.password}
						color="primary"
						variant="outlined"
						fullWidth
						label="Password"
						placeholder="password"
					/>
				</Box>
				<Box textAlign="center">
					<Button variant="contained" color="primary" type="submit">
						Login
					</Button>
				</Box>
			</form>
		</div>
	);
}
