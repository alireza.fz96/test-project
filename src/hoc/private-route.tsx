import { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import AuthContext from '../contexts/auth-context';

export default function PrivateRoute({ children, ...rest }: any) {
	const { isAuthenticated } = useContext(AuthContext);

	return (
		<Route
			{...rest}
			render={({ location }) =>
				isAuthenticated ? (
					children
				) : (
					<Redirect
						to={{
							pathname: '/login',
							state: { from: location }
						}}
					/>
				)
			}
		/>
	);
}
