import { ReactChild, useState } from 'react';
import AuthContext from '../contexts/auth-context';

interface Props {
	children: ReactChild;
}

const AUTH_KEY = 'AUTH_KEY';

function setToken(token: string) {
	localStorage.setItem(AUTH_KEY, token);
}

function getToken() {
	return localStorage.getItem(AUTH_KEY);
}

function deleteToken() {
	localStorage.removeItem(AUTH_KEY);
}

export default function AuthProviders(props: Props) {
	const [isAuthenticated, setIsAuthenticated] = useState<boolean>(!!getToken());

	const login = (email: string, password: string) => {
		const user = { email, password };
		setToken(JSON.stringify(user));
		setIsAuthenticated(true);
	};

	const logout = () => {
		deleteToken();
		setIsAuthenticated(false);
	};

	return (
		<AuthContext.Provider value={{ isAuthenticated, logout, login }}>
			{props.children}
		</AuthContext.Provider>
	);
}
