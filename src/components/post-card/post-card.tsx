import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Post } from '../../models/Post';

interface Props {
	post: Post;
}

const useStyles = makeStyles({
	root: {
		height: 190
	},
	body: {
		maxHeight: 38,
		overflow: 'hidden',
		textOverflow: 'ellipsis'
	},
	title: {
		height: 45,
		overflow: 'hidden',
		textOverflow: 'ellipsis',
		whiteSpace: 'nowrap'
	}
});

export default function PostCard(props: Props) {
	const classes = useStyles();

	const { title, body } = props.post;

	return (
		<Card className={classes.root}>
			<CardContent>
				<Typography
					gutterBottom
					variant="h5"
					component="h2"
					className={classes.title}
				>
					{title}
				</Typography>
				<Typography
					variant="body2"
					color="textSecondary"
					component="p"
					className={classes.body}
				>
					{body}
				</Typography>
			</CardContent>
			<CardActions>
				<Button size="small" color="primary">
					Share
				</Button>
			</CardActions>
		</Card>
	);
}
