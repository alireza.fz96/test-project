import { useEffect } from 'react';
import Nprogress from 'nprogress';

export default function ProgressBar() {
	useEffect(() => {
		Nprogress.start();
		return () => {
			Nprogress.done();
		};
	}, []);
	return null;
}
