import { InputAdornment, TextField } from '@material-ui/core';
import { Search } from '@material-ui/icons';
import { memo } from 'react';
import { DebounceInput } from 'react-debounce-input';

interface Props {
	onChange: any;
}

function Input(props: any) {
	return (
		<TextField
			{...props}
			fullWidth
			label="Search"
			placeholder="Type something ..."
			InputProps={{
				endAdornment: (
					<InputAdornment position="end">
						<Search />
					</InputAdornment>
				)
			}}
			inputMode="search"
		/>
	);
}

function SearchBar(props: Props) {
	return (
		<div>
			<DebounceInput
				debounceTimeout={400}
				minLength={2}
				onChange={props.onChange}
				element={Input}
			/>
		</div>
	);
}

export default memo(SearchBar);
