import './App.css';
import { Redirect, Route, Switch } from 'react-router-dom';
import { lazy, Suspense, useContext } from 'react';
import PrivateRoute from './hoc/private-route';
import AuthContext from './contexts/auth-context';
import ProgressBar from './components/progress-bar/progress-bar';

const Login = lazy(() => import('./pages/login/login'));
const Posts = lazy(() => import('./pages/posts/posts'));

function App() {
	const { isAuthenticated } = useContext(AuthContext);

	return (
		<Suspense fallback={<ProgressBar />}>
			<Switch>
				{!isAuthenticated && <Route component={Login} path="/login" />}
				<PrivateRoute path="/posts">
					<Posts />
				</PrivateRoute>
				<Redirect to="/posts" />
			</Switch>
		</Suspense>
	);
}

export default App;
