import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'nprogress/nprogress.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter as Router } from 'react-router-dom';
import AuthProviders from './hoc/auth-provider';

ReactDOM.render(
	<React.StrictMode>
		<AuthProviders>
			<Router>
				<App />
			</Router>
		</AuthProviders>
	</React.StrictMode>,
	document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
