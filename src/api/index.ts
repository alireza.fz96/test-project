import { Post } from '../models/Post';
import axios from './axios-base';

export default function getPosts(page: number, searchText: string) {
	const url = searchText
		? `/posts?_page=${page}&title=${searchText}`
		: `/posts?_page=${page}`;
	return axios.get<Post[]>(url).then(data => data.data);
}
